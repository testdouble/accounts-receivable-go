# Accounts Receivable

Welcome to the Accounts Receivable system! This system is responsible for
creating, sending, and tracking invoices sent to clients so we can be paid for
our products and services.

## Getting Started

This app is a Go api app built using [Gin](https://github.com/gin-gonic/gin) (A popular HTTP framework) and [gorm](https://gorm.io/) (A popular Go ORM).

### Prerequisites

Before you get started, be sure to:

- Have go 1.21 installed and running. Use `go version` to see your local version.

### Initial Setup

Install dependencies.

```
$ make install
```

### Tests

To run tests. Run the command below:

```
$ make test
```

### Build

To build a binary for the app you can run the command below. It will put the binary in the `./dist` folder in the project root.

```
$ make build
```

### Run

To build and run the binary:

```
$ make run
```
