#!/usr/bin/env bash

set -euo pipefail

case "$OSTYPE" in
  darwin*)  brew install sqlite3 ;; 
  *)        echo "unknown: $OSTYPE" ;;
esac
