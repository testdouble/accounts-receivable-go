package main

import (
	"log"
	"os"

	"gitlab.com/testdouble/accounts-receivable-go/internal/db"
	"gitlab.com/testdouble/accounts-receivable-go/internal/server/http"
)

func main() {
	db, err := db.Initialize()

	if err != nil {
		log.Fatalf("goose: failed to migrate: %v", err)
		os.Exit(1)
	}

	cfg := &http.Config{
		Port: "8080",
		DB:   db,
	}

	httpSrv := http.NewService(cfg)
	if err := httpSrv.Start(); err != nil {
		log.Fatalf("http server failed to start: %v", err)
	}
}
