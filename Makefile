install:
	./scripts/sqlite-install.sh
	CGO_ENABLED=1 go install github.com/mattn/go-sqlite3

clean:
	rm -f accounts-receivable.db
	find ./dist -type f -not -name '*.gitkeep' -delete

build:
	${MAKE} clean
	CGO_ENABLED=1 go build -v -o ./dist/accounts-receivable main.go

test:
	CGO_ENABLED=1 go test ./...

run:
	${MAKE} build
	./dist/accounts-receivable

