package db

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type Client struct {
	gorm.Model
	Name  string
	Email string
}
type Invoice struct {
	gorm.Model
	NetDays   uint `gorm:"default:30;"`
	Note      string
	ClientId  uint
	Client    Client
	LineItems []LineItem
}
type LineItem struct {
	gorm.Model
	InvoiceId          uint
	Invoice            Invoice
	ProductId          uint
	Product            Product
	ServiceId          uint
	Service            Service
	Description        string
	PriceOverrideCents uint
	Quantity           uint `gorm:"default:1"`
}
type Product struct {
	gorm.Model
	Name       string
	PriceCents uint
}
type Service struct {
	gorm.Model
	Name       string
	PriceCents uint
}

func Initialize() (*gorm.DB, error) {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	// Migrate the schema
	db.AutoMigrate(&Client{})
	db.AutoMigrate(&Product{})
	db.AutoMigrate(&Service{})
	db.AutoMigrate(&LineItem{})
	db.AutoMigrate(&Invoice{})

	// Clean any existing data from the tables
	db.Where("1 = 1").Delete(&Product{})
	db.Where("1 = 1").Delete(&Service{})
	db.Where("1 = 1").Delete(&Invoice{})
	db.Where("1 = 1").Delete(&LineItem{})
	db.Where("1 = 1").Delete(&Client{})

	client, err := SeedClient(db)
	if err != nil {
		return nil, err
	}

	SeedInvoices(db, client)

	return db, nil
}

func SeedClient(db *gorm.DB) (*Client, error) {
	client := &Client{
		Name:  "Acme",
		Email: "wile.e.coyote@acme.com",
	}

	res := db.Create(client)

	return client, res.Error
}

func SeedInvoices(db *gorm.DB, client *Client) {
	programmingService := Service{
		Name:       "Programming",
		PriceCents: 8000,
	}
	refactoringService := Service{
		Name:       "Refactoring",
		PriceCents: 35000,
	}

	db.Create(&programmingService)
	db.Create(&refactoringService)

	serverlessProduct := Product{
		Name:       "Serverless Servers",
		PriceCents: 100000,
	}

	bitcoinMining := Product{
		Name:       "Bitcoin Mining",
		PriceCents: 20000,
	}

	db.Create(&serverlessProduct)
	db.Create(&bitcoinMining)

	initialInvoice := &Invoice{
		ClientId: client.ID,
		Note:     "Invoice for writing software for Acme",
		LineItems: []LineItem{
			{
				ServiceId:   programmingService.ID,
				Quantity:    40,
				Description: "Amber's quality programming",
			},
			{
				ServiceId:   programmingService.ID,
				Quantity:    40,
				Description: "Justin's discount programming",
			},
			{
				ProductId:   serverlessProduct.ID,
				Quantity:    20,
				Description: "Enough serverless servers to achieve web scale",
			},
		},
	}

	inv := db.Create(initialInvoice)

	if inv.Error != nil {
		panic(inv.Error)
	}

	refactorInvoice := &Invoice{
		ClientId: client.ID,
		Note:     "Invoice for refactoring software and mining for Acme",
		LineItems: []LineItem{
			{
				ServiceId:   refactoringService.ID,
				Quantity:    40,
				Description: "Megan fixing previous discount programming",
			},
			{
				ProductId:   bitcoinMining.ID,
				Quantity:    100,
				Description: "100 hours of bitcoing mining",
			},
		},
	}

	db.Create(refactorInvoice)
}
