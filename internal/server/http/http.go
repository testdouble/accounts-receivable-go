package http

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type HTTPServer struct {
	router *gin.Engine
	cfg    *Config
}

type Config struct {
	Host string
	Port string
	DB   *gorm.DB
}

func (h *HTTPServer) Start() error {
	addr := fmt.Sprintf("%s:%s", h.cfg.Host, h.cfg.Port)
	return h.router.Run(addr) // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

func NewService(cfg *Config) *HTTPServer {
	r := gin.New()

	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	routes := Routes{cfg.DB}

	r.GET("/ping", routes.Pong)

	r.GET("/invoices", routes.ListInvoices)
	r.POST("/invoices", routes.CreateInvoice)
	r.GET("/invoices/:invoiceId", routes.GetInvoice)
	r.POST("/invoices/:invoiceId", routes.UpdateInvoice)
	r.DELETE("/invoices/:invoiceId", routes.DeleteInvoice)

	r.GET("/clients", routes.ListClients)
	r.POST("/clients", routes.CreateClient)
	r.GET("/clients/:clientId", routes.GetClient)
	r.POST("/clients/:clientId", routes.UpdateClient)
	r.DELETE("/clients/:clientId", routes.DeleteClient)

	r.GET("/products", routes.ListProducts)
	r.POST("/products", routes.CreateProduct)
	r.GET("/products/:productId", routes.GetProduct)
	r.POST("/products/:productId", routes.UpdateProduct)
	r.DELETE("/products/:productId", routes.DeleteProduct)

	r.GET("/services", routes.ListServices)
	r.POST("/services", routes.CreateService)
	r.GET("/services/:serviceId", routes.GetService)
	r.POST("/services/:serviceId", routes.UpdateService)
	r.DELETE("/services/:serviceId", routes.DeleteService)

	return &HTTPServer{
		r,
		cfg,
	}
}
