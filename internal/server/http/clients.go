package http

import (
	"gitlab.com/testdouble/accounts-receivable-go/internal/db"

	"github.com/gin-gonic/gin"
)

func (r *Routes) ListClients(c *gin.Context) {
	var clients []db.Client
	if err := r.DB.Find(&clients).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, clients)
}

func (r *Routes) GetClient(c *gin.Context) {
	clientId, ok := c.Params.Get("clientId")

	if !ok {
		c.JSON(400, gin.H{
			"error": "clientId is missing in route params",
		})
		return
	}

	var client db.Client
	if err := r.DB.First(&client, clientId).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, client)
}

func (r *Routes) CreateClient(c *gin.Context) {
	var client db.Client

	if err := c.ShouldBind(&client); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	if err := r.DB.Create(client).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, client)
}

func (r *Routes) UpdateClient(c *gin.Context) {
	clientId, ok := c.Params.Get("clientId")
	if !ok {
		c.JSON(400, gin.H{
			"error": "clientId is missing in route params",
		})
		return
	}

	var client db.Client

	if err := r.DB.Where("id = ?", clientId).First(&client).Error; err != nil {
		c.JSON(404, gin.H{
			"error": err,
		})
		return
	}

	var clientUpd db.Client

	if err := c.ShouldBind(&clientUpd); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	client.Email = clientUpd.Email
	client.Name = clientUpd.Name

	if err := r.DB.Save(&client).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, client)
}

func (r *Routes) DeleteClient(c *gin.Context) {

	clientId, ok := c.Params.Get("clientId")
	if !ok {
		c.JSON(400, gin.H{
			"error": "clientId is missing in route params",
		})
		return
	}

	if err := r.DB.Where("id = ?", clientId).Delete(&db.Client{}); err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.String(200, "")
}
