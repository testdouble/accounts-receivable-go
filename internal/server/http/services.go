package http

import (
	"gitlab.com/testdouble/accounts-receivable-go/internal/db"

	"github.com/gin-gonic/gin"
)

func (r *Routes) ListServices(c *gin.Context) {
	var services []db.Service
	if err := r.DB.Find(&services).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, services)
}

func (r *Routes) GetService(c *gin.Context) {
	serviceId, ok := c.Params.Get("serviceId")
	if !ok {
		c.JSON(400, gin.H{
			"error": "serviceId is missing in route params",
		})
		return
	}

	var service db.Service
	if err := r.DB.First(&service, serviceId).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, service)
}

func (r *Routes) CreateService(c *gin.Context) {
	var service db.Service

	if err := c.ShouldBind(&service); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	if err := r.DB.Create(service).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, service)
}

func (r *Routes) UpdateService(c *gin.Context) {
	serviceId, ok := c.Params.Get("serviceId")

	if !ok {
		c.JSON(400, gin.H{
			"error": "serviceId is missing in route params",
		})
		return
	}

	var service db.Service

	if err := r.DB.Where("id = ?", serviceId).First(&service).Error; err != nil {
		c.JSON(404, gin.H{
			"error": err,
		})
		return
	}

	var serviceUpd db.Service

	if err := c.ShouldBind(&serviceUpd); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	service.Name = serviceUpd.Name
	service.PriceCents = serviceUpd.PriceCents

	if err := r.DB.Save(&service).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, service)
}

func (r *Routes) DeleteService(c *gin.Context) {
	serviceId, ok := c.Params.Get("serviceId")

	if !ok {
		c.JSON(400, gin.H{
			"error": "serviceId is missing in route params",
		})
		return
	}

	if err := r.DB.Where("id = ?", serviceId).Delete(&db.Service{}); err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.String(200, "")
}
