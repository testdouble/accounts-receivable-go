package http

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Routes struct {
	DB *gorm.DB
}

func (r *Routes) Pong(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}
