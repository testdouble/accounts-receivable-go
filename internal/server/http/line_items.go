package http

import (
	"gitlab.com/testdouble/accounts-receivable-go/internal/db"

	"github.com/gin-gonic/gin"
)

func (r *Routes) ListLineItem(c *gin.Context) {
	var lineItems []db.LineItem
	if err := r.DB.Find(&lineItems).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, lineItems)
}

func (r *Routes) GetLineItem(c *gin.Context) {
	lineItemId, ok := c.Params.Get("lineItemId")
	if !ok {
		c.JSON(400, gin.H{
			"error": "lineItemId is missing in route params",
		})
		return
	}

	var lineItem db.LineItem
	if err := r.DB.First(&lineItem, lineItemId).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, lineItem)
}

func (r *Routes) CreateLineItem(c *gin.Context) {
	var lineItem db.LineItem

	if err := c.ShouldBind(&lineItem); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	if err := r.DB.Create(lineItem).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, lineItem)
}

func (r *Routes) UpdateLineItem(c *gin.Context) {
	lineItemId, ok := c.Params.Get("lineItemId")

	if !ok {
		c.JSON(400, gin.H{
			"error": "lineItemId is missing in route params",
		})
		return
	}

	var lineItem db.LineItem

	if err := r.DB.Where("id = ?", lineItemId).First(&lineItem).Error; err != nil {
		c.JSON(404, gin.H{
			"error": err,
		})
		return
	}

	var lineItemUpd db.LineItem

	if err := c.ShouldBind(&lineItemUpd); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	lineItem.InvoiceId = lineItemUpd.InvoiceId
	lineItem.ProductId = lineItemUpd.ProductId
	lineItem.ServiceId = lineItemUpd.ServiceId
	lineItem.Description = lineItemUpd.Description
	lineItem.PriceOverrideCents = lineItemUpd.PriceOverrideCents
	lineItem.Quantity = lineItemUpd.Quantity

	if err := r.DB.Save(&lineItem).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, lineItem)
}

func (r *Routes) DeleteLineItem(c *gin.Context) {
	lineItemId, ok := c.Params.Get("lineItemId")

	if !ok {
		c.JSON(400, gin.H{
			"error": "lineItemId is missing in route params",
		})
		return
	}

	if err := r.DB.Where("id = ?", lineItemId).Delete(&db.LineItem{}); err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.String(200, "")
}
