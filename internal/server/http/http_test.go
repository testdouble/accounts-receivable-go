package http

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/testdouble/accounts-receivable-go/internal/db"
	"gorm.io/gorm"

	"github.com/stretchr/testify/assert"
)

var DB *gorm.DB

func init() {
	var d, err = db.Initialize()
	if err != nil {
		panic(err)
	}

	DB = d
}

func TestPingRoute(t *testing.T) {
	httpSrv := NewService(&Config{
		Port: "8080",
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	httpSrv.router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.JSONEq(t, `{"message":"pong"}`, w.Body.String())
}

func TestGetInvoices(t *testing.T) {
	httpSrv := NewService(&Config{
		Port: "8080",
		DB:   DB,
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/invoices", nil)
	httpSrv.router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	var invoices []db.Invoice

	err := json.Unmarshal(w.Body.Bytes(), &invoices)

	assert.NoError(t, err)
	assert.Equal(t, invoices[0].ID, uint(1))
	assert.Equal(t, invoices[0].NetDays, uint(30))
	assert.Equal(t, invoices[0].Note, "Invoice for writing software for Acme")
	assert.Equal(t, invoices[1].ID, uint(2))
	assert.Equal(t, invoices[1].NetDays, uint(30))
	assert.Equal(t, invoices[1].Note, "Invoice for refactoring software and mining for Acme")
}

func TestGetClients(t *testing.T) {
	httpSrv := NewService(&Config{
		Port: "8080",
		DB:   DB,
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/clients", nil)
	httpSrv.router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	var clients []db.Client

	err := json.Unmarshal(w.Body.Bytes(), &clients)

	assert.NoError(t, err)
	assert.Equal(t, clients[0].ID, uint(1))
	assert.Equal(t, clients[0].Email, "wile.e.coyote@acme.com")
	assert.Equal(t, clients[0].Name, "Acme")
}

func TestGetProducts(t *testing.T) {
	httpSrv := NewService(&Config{
		Port: "8080",
		DB:   DB,
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/products", nil)
	httpSrv.router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	var products []db.Product

	err := json.Unmarshal(w.Body.Bytes(), &products)

	assert.NoError(t, err)
	assert.Equal(t, products[0].ID, uint(1))
	assert.Equal(t, products[0].Name, "Serverless Servers")
	assert.Equal(t, products[1].ID, uint(2))
	assert.Equal(t, products[1].Name, "Bitcoin Mining")
}
