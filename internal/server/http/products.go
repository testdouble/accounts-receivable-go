package http

import (
	"gitlab.com/testdouble/accounts-receivable-go/internal/db"

	"github.com/gin-gonic/gin"
)

func (r *Routes) ListProducts(c *gin.Context) {

	var products []db.Product
	if err := r.DB.Find(&products).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, products)
}

func (r *Routes) GetProduct(c *gin.Context) {
	productId, ok := c.Params.Get("productId")
	if !ok {
		c.JSON(400, gin.H{
			"error": "productId is missing in route params",
		})
		return
	}

	var product db.Product
	if err := r.DB.First(&product, productId).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, product)
}

func (r *Routes) CreateProduct(c *gin.Context) {
	var product db.Product

	if err := c.ShouldBind(&product); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	if err := r.DB.Create(product).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, product)
}

func (r *Routes) UpdateProduct(c *gin.Context) {
	productId, ok := c.Params.Get("productId")

	if !ok {
		c.JSON(400, gin.H{
			"error": "productId is missing in route params",
		})
		return
	}

	var product db.Product

	if err := r.DB.Where("id = ?", productId).First(&product).Error; err != nil {
		c.JSON(404, gin.H{
			"error": err,
		})
		return
	}

	var productUpd db.Product

	if err := c.ShouldBind(&productUpd); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	product.Name = productUpd.Name
	product.PriceCents = productUpd.PriceCents

	if err := r.DB.Save(&product).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, product)
}

func (r *Routes) DeleteProduct(c *gin.Context) {
	productId, ok := c.Params.Get("productId")

	if !ok {
		c.JSON(400, gin.H{
			"error": "productId is missing in route params",
		})
		return
	}

	if err := r.DB.Where("id = ?", productId).Delete(&db.Product{}); err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.String(200, "")
}
