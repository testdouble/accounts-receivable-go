package http

import (
	"gitlab.com/testdouble/accounts-receivable-go/internal/db"

	"github.com/gin-gonic/gin"
)

func (r *Routes) ListInvoices(c *gin.Context) {

	var invoices []db.Invoice
	err := r.DB.
		Preload("Client").
		Preload("LineItems.Product").
		Preload("LineItems.Service").
		Find(&invoices).Error

	if err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, invoices)
}

func (r *Routes) GetInvoice(c *gin.Context) {

	invoiceId, ok := c.Params.Get("invoiceId")
	if !ok {
		c.JSON(400, gin.H{
			"error": "invoiceId is missing in route params",
		})
		return
	}

	var invoice db.Invoice
	err := r.DB.
		Preload("Client").
		Preload("LineItems.Product").
		Preload("LineItems.Service").
		First(&invoice, invoiceId).Error

	if err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	total := uint(0)

	for _, lineItem := range invoice.LineItems {
		if lineItem.Product.ID != 0 {
			total += lineItem.Product.PriceCents * lineItem.Quantity
		}

		if lineItem.Service.ID != 0 {
			total += lineItem.Service.PriceCents * lineItem.Quantity
		}
	}

	c.JSON(200, gin.H{
		"client":  invoice.Client,
		"invoice": invoice,
		"total":   total,
	})
}

func (r *Routes) CreateInvoice(c *gin.Context) {

	var invoice db.Invoice

	if err := c.ShouldBind(&invoice); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	if err := r.DB.Create(invoice).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, invoice)
}

func (r *Routes) UpdateInvoice(c *gin.Context) {

	invoiceId, ok := c.Params.Get("invoiceId")
	if !ok {
		c.JSON(400, gin.H{
			"error": "invoiceId is missing in route params",
		})
		return
	}

	var invoice db.Invoice

	if err := r.DB.Where("id = ?", invoiceId).First(&invoice).Error; err != nil {
		c.JSON(404, gin.H{
			"error": err,
		})
		return
	}

	var invoiceUpd db.Invoice

	if err := c.ShouldBind(&invoiceUpd); err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}

	invoice.NetDays = invoiceUpd.NetDays
	invoice.Note = invoiceUpd.Note

	if err := r.DB.Save(&invoice).Error; err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(200, invoice)
}

func (r *Routes) DeleteInvoice(c *gin.Context) {

	invoiceId, ok := c.Params.Get("invoiceId")
	if !ok {
		c.JSON(400, gin.H{
			"error": "invoiceId is missing in route params",
		})
		return
	}

	if err := r.DB.Where("id = ?", invoiceId).Delete(&db.Invoice{}); err != nil {
		c.JSON(500, gin.H{
			"error": err,
		})
		return
	}

	c.String(200, "")
}
